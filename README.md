# Riot.js movie browser sample

A demo movie browser component built with Riot.js featuring basic search
filtering, sorting, pagination and simple state. Uses minimal plain CSS 
applied to achieve basic layout.

## Online demo

[http://bitnbyte.bitbucket.org/riotjs-movies-app](http://bitnbyte.bitbucket.org/riotjs-movies-app)
